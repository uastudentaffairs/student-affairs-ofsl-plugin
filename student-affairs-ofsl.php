<?php

/**
* Plugin Name:       Student Affairs - The Office of Fraternity and Sorority Life
* Plugin URI:        https://sa.ua.edu
* Description:       This WordPress plugin is intended solely for The University of Alabama Division of Student Affairs.
* Version:           1.0
* Author:            UA Division of Student Affairs - Rachel Carden
* Author URI:        https://sa.ua.edu
*/

// Include files
require_once plugin_dir_path( __FILE__ ) . 'includes/filters.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/shortcodes.php';

// Add admin functionality in admin
if( is_admin() ) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/admin.php';
}

//! Setup styles and scripts
add_action( 'wp_enqueue_scripts', function () {

	// Get the plugin CSS directory
	$sa_ofsl_dir = plugin_dir_url( __FILE__ ) . 'css/';

	// Enqueue our main styles
	wp_enqueue_style( 'sa-ofsl', $sa_ofsl_dir . 'sa-ofsl.css', array( 'sa-child' ), false );

}, 30 );

// Add image sizes
add_action( 'after_setup_theme', 'setup_sa_child_theme_image_sizes', 10 );
function setup_sa_child_theme_image_sizes() {

	add_image_size( 'sa-ofsl-councils-grid', 600, 250, true );
	add_image_size( 'sa-page-featured-image', 910, 320, true );

}

// Register OFSL custom post types
add_action( 'init', 'ua_sa_ofsl_register_cpt' );
function ua_sa_ofsl_register_cpt() {

	// Register chapters CPT
	register_post_type( 'chapters', array(
		'labels' => array(
			'name'               => _x( 'Chapters', 'post type general name', 'ua-sa-career' ),
			'singular_name'      => _x( 'Chapter', 'post type singular name', 'ua-sa-career' ),
			'menu_name'          => _x( 'Chapters', 'admin menu', 'ua-sa-career' ),
			'name_admin_bar'     => _x( 'Chapters', 'add new on admin bar', 'ua-sa-career' ),
			'add_new'            => _x( 'Add New', 'chapters', 'ua-sa-career' ),
			'add_new_item'       => __( 'Add New Chapter', 'ua-sa-career' ),
			'new_item'           => __( 'New Chapter', 'ua-sa-career' ),
			'edit_item'          => __( 'Edit Chapter', 'ua-sa-career' ),
			'view_item'          => __( 'View Chapter', 'ua-sa-career' ),
			'all_items'          => __( 'All Chapters', 'ua-sa-career' ),
			'search_items'       => __( 'Search Chapters', 'ua-sa-career' ),
			'parent_item_colon'  => __( 'Parent Chapter:', 'ua-sa-career' ),
			'not_found'          => __( 'No chapters found.', 'ua-sa-career' ),
			'not_found_in_trash' => __( 'No chapters found in Trash.', 'ua-sa-career' )
		),
		'public'                => true,
		'hierarchical'          => false,
		'exclude_from_search'   => false,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-universal-access-alt',
		'capabilities'          => array(
			'edit_post'         => 'edit_chapter',
			'read_post'         => 'read_chapter',
			'delete_post'       => 'delete_chapter',
			'edit_posts'        => 'edit_chapters',
			'edit_others_posts' => 'edit_others_chapters',
			'publish_posts'     => 'publish_chapters',
			'read_private_posts'=> 'read_private_chapters',
			'read'              => 'read',
			'delete_posts'      => 'delete_chapters',
			'delete_private_posts' => 'delete_private_chapters',
			'delete_published_posts' => 'delete_published_chapters',
			'delete_others_posts' => 'delete_others_chapters',
			'edit_private_posts' => 'edit_private_chapters',
			'edit_published_posts' => 'edit_published_chapters',
			'create_posts'      => 'edit_chapters'
		),
		'hierarchical'          => false,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'has_archive'           => false,
		'rewrite'               => array(
			'slug'  => 'chapters',
			'feeds' => false,
			'pages' => true,
		),
		'query_var'             => true,
	) );

	// Register councils CPT
	register_post_type( 'councils', array(
		'labels' => array(
			'name'               => _x( 'Councils', 'post type general name', 'ua-sa-career' ),
			'singular_name'      => _x( 'Council', 'post type singular name', 'ua-sa-career' ),
			'menu_name'          => _x( 'Councils', 'admin menu', 'ua-sa-career' ),
			'name_admin_bar'     => _x( 'Councils', 'add new on admin bar', 'ua-sa-career' ),
			'add_new'            => _x( 'Add New', 'councils', 'ua-sa-career' ),
			'add_new_item'       => __( 'Add New Council', 'ua-sa-career' ),
			'new_item'           => __( 'New Council', 'ua-sa-career' ),
			'edit_item'          => __( 'Edit Council', 'ua-sa-career' ),
			'view_item'          => __( 'View Council', 'ua-sa-career' ),
			'all_items'          => __( 'All Councils', 'ua-sa-career' ),
			'search_items'       => __( 'Search Councils', 'ua-sa-career' ),
			'parent_item_colon'  => __( 'Parent Council:', 'ua-sa-career' ),
			'not_found'          => __( 'No councils found.', 'ua-sa-career' ),
			'not_found_in_trash' => __( 'No councils found in Trash.', 'ua-sa-career' )
		),
		'public'                => true,
		'hierarchical'          => false,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-groups',
		'capabilities'          => array(
			'edit_post'         => 'edit_council',
			'read_post'         => 'read_council',
			'delete_post'       => 'delete_council',
			'edit_posts'        => 'edit_councils',
			'edit_others_posts' => 'edit_others_councils',
			'publish_posts'     => 'publish_councils',
			'read_private_posts'=> 'read_private_councils',
			'read'              => 'read',
			'delete_posts'      => 'delete_councils',
			'delete_private_posts' => 'delete_private_councils',
			'delete_published_posts' => 'delete_published_councils',
			'delete_others_posts' => 'delete_others_councils',
			'edit_private_posts' => 'edit_private_councils',
			'edit_published_posts' => 'edit_published_councils',
			'create_posts'      => 'edit_councils'
		),
		'hierarchical'          => false,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'has_archive'           => false,
		'rewrite'               => array(
			'slug'  => 'councils',
			'feeds' => false,
			'pages' => true,
		),
		'query_var'             => true,
	) );

}

// Print address - whether physical or mailing
function sa_ofsl_print_address( $post_id, $address_type = 'physical' ) {

	if ( $address_string = sa_ofsl_get_address( $post_id, $address_type ) ) {
		echo $address_string;
	}

}

// Get address string - whether physical or mailing
function sa_ofsl_get_address( $post_id, $address_type = 'physical' ) {

	// Make sure we have a valid address type
	if ( ! in_array( $address_type, array( 'mailing', 'physical' ) ) ) {
		return false;
	}

	// Get fields
	$address1         = get_post_meta( $post_id, "{$address_type}_address", true );
	$address2         = get_post_meta( $post_id, "{$address_type}_address_2", true );
	$address_city     = get_post_meta( $post_id, "{$address_type}_address_city", true );
	$address_state    = get_post_meta( $post_id, "{$address_type}_address_state", true );
	$address_zip_code = get_post_meta( $post_id, "{$address_type}_address_zip_code", true );

	// Build address
	$address_string = null;

	if ( $address1 ) {

		$address_string = '<div class="address address-' . $address_type . '">';

		$address_string .= '<span class="address1">' . $address1 . '</span>';

		if ( $address2 ) {
			$address_string .= ' <span class="address2">' . $address2 . '</span>';
		}

		if ( $address_city ) {
			$address_string .= ' <span class="address-city">' . $address_city . '</span>';
		}

		if ( $address_state ) {
			$address_string .= ' <span class="address-state">' . $address_state . '</span>';
		}

		if ( $address_zip_code ) {
			$address_string .= ' <span class="address-zip">' . $address_zip_code . '</span>';
		}

		$address_string .= '</div>';

	}

	return $address_string;

}