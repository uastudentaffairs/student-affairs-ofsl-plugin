<?php

//! Print Councils grid
add_shortcode( 'print_sa_ofsl_councils', function( $args, $content = null ) {

	// Setup the args
	//$defaults = array();
	//$args = wp_parse_args( $args, $defaults );

	// Build content
	$councils_content = null;

	// Setup arguments for the query
	$council_args = array(
		'posts_per_page'   => -1,
		'orderby'          => 'title',
		'order'            => 'ASC',
		'post_type'        => 'councils',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);

	// Get the councils
	$councils = new WP_Query( $council_args );

	// Build the council count
	if ( $councils->have_posts() ) {

		while ( $councils->have_posts() ) {
			$councils->the_post();

			// Get the title and permalink
			$council_id = get_the_ID();
			$council_title = get_the_title();
			$council_alias = get_post_meta( $council_id, 'council_alias', true );
			$council_permalink = get_permalink( $council_id );

			// Build the council content
			$council_content = null;

			// Get the image
			$council_image_src = ( $council_image = wp_get_attachment_image_src( get_post_thumbnail_id( $council_id ), 'sa-ofsl-councils-grid' ) ) && isset( $council_image[0] ) ? $council_image[0] : false;

			// Add the image
			if ( $council_image_src ) {
				$council_content .= '<a href="' . $council_permalink . '"><img src="' . $council_image_src . '" /></a>';
			}

			// Add the title
			$council_content .= '<h2 class="item-title council-title"><a href="' . $council_permalink . '">' . $council_title . '</a></h2>';

			// Add the excerpt
			if ( $council_excerpt = $councils->post->post_excerpt ) {
				$council_content .= wpautop( $council_excerpt );
			}

			// Create list of links
			$council_links = array();

			// Add the link to the council's page
			$council_links[] = '<a href="' . $council_permalink . '">Learn more about the ' . ( $council_alias ? $council_alias : $council_title ) . '</a>';

			// Add the link to the council's chapters
			$council_links[] = '<a href="' . $council_permalink . '#chapters">View this council\'s chapters</a>';

			// Add the links
			$council_content .= '<ul class="council-links"><li>' . implode( '</li><li>', $council_links ) . '</li></ul>';

			// Wrap the council and add to the mix
			$councils_content .= '<li class="item council">' . $council_content . '</li>';

		}

		// Wrap the content
		// @TODO remove 'sa-content-image-grid' when moved to new theme
		$councils_content = '<ul class="sa-content-image-grid sa-ofsl-councils small-block-grid-1 medium-block-grid-2">' . $councils_content . '</ul>';

	}

	// Restore original Post Data
	wp_reset_postdata();

	return $councils_content;

});

//! Print Chapters grid
add_shortcode( 'print_sa_ofsl_chapters', function( $args, $content = null ) {

	// Setup the args
	$defaults = array(
		'councils'          => null,
		'display_header'    => false,
		'header'            => 'University of Alabama Chapters',
	);
	$args = wp_parse_args( $args, $defaults );

	// Setup arguments for the query
	$chapter_args = array(
		'posts_per_page'   => -1,
		'orderby'          => 'title',
		'order'            => 'ASC',
		'post_type'        => 'chapters',
		'post_status'      => 'publish',
		'suppress_filters' => true
	);

	// Setup display_header argument - false by default
	if ( ! empty( $args['display_header'] ) && ( true === $args['display_header'] || $args['display_header'] > 0 || strcasecmp( 'true', $args['display_header'] ) == 0 ) ) {
		$args['display_header'] = true;
	} else {
		$args['display_header'] = false;
	}

	// If we're supposed to be limiting councils, setup the query
	if ( ! empty( $args[ 'councils' ] ) ) {

		// Make sure it's an array
		if ( ! is_array( $args[ 'councils' ] ) ) {
			$args[ 'councils' ]  = explode( ',', preg_replace( '/\s/i', '', $args[ 'councils' ] ) );
		}

		if ( ! empty( $args[ 'councils' ] ) ) {

			// Setup meta query
			$chapter_args[ 'meta_query' ] = array();

			foreach( $args[ 'councils' ] as $council_id ) {
				$chapter_args[ 'meta_query' ][] = array(
					'key'		=> 'chapter_council',
					'value'		=> $council_id,
					'compare'	=> 'LIKE'
				);
			}

			// Setup an "OR" relationship
			$chapter_args[ 'meta_query' ][ 'relation' ] = 'OR';

		}

	}

	// Get the chapters
	$chapters = new WP_Query( $chapter_args );

	// Build content
	$chapters_content = null;

	// Remove the "Read More" filter
	remove_filter( 'wp_trim_excerpt', 'sa_framework_add_read_more_to_excerpt', 10 );

	// Should we show council name in list? - don't show if only one council
	$show_council_name = ! ( ! empty( $args[ 'councils' ] ) && count( $args[ 'councils' ] ) == 1 );

	// Build the chapter count
	if ( $chapters->have_posts() ) {

		while ( $chapters->have_posts() ) {
			$chapters->the_post();

			// Get some chapter post info
			$chapter_id = get_the_ID();
			$chapter_title = get_the_title();
			$chapter_permalink = get_permalink( $chapter_id );

			// Get some chapter info
			$chapter_councils = get_post_meta( $chapter_id, 'chapter_council', true );
			//$chapter_type = get_post_meta( $chapter_id, 'chapter_type', true );

			// Get the chapter crest
			$chapter_crest = get_post_meta( $chapter_id, 'chapter_crest', true );
			$chapter_crest_image_src = 'https://placehold.it/300x300';
			if ( $chapter_crest > 0 ) {
				$chapter_crest_image_src = ( $chapter_crest_image = wp_get_attachment_image_src( $chapter_crest, 'thumbnail' ) ) && isset( $chapter_crest_image[0] ) ? $chapter_crest_image[0] : $chapter_crest_image_src;
			}

			// Build the chapter content
			$chapter_content = null;

			// Add the image
			$chapter_content .= '<a href="' . $chapter_permalink . '"><img class="crest" src="' . $chapter_crest_image_src . '" /></a>';

			// Add the chapter info wrapper
			$chapter_content .= '<div class="chapter-info">';

				// Add the title
				$chapter_content .= '<h3 class="chapter-title"><a href="' . $chapter_permalink . '">' . $chapter_title . '</a></h3>';

				// If we have the council
				if ( $show_council_name && $chapter_councils ) {
					$chapter_council_string = array();
					foreach( $chapter_councils as $council_id ) {
						$chapter_council_string[] = get_the_title( $council_id );
					}
					$chapter_content .= '<div class="chapter-details"><span class="chapter-council">' . implode( ',', $chapter_council_string ) . '</span></div>';
				}

				// Add the excerpt
				if ( $chapter_excerpt = get_the_excerpt() ) {
					$chapter_content .= '<div class="chapter-excerpt">' . wpautop( $chapter_excerpt ) . '</div>';
				}

				// Add the link to the chapter's page
				$chapter_content .= '<a href="' . $chapter_permalink . '">Learn more about ' . $chapter_title . '</a>';

			// Close the chapter info
			$chapter_content .= '</div>';

			// Wrap the chapter and add to the mix
			$chapters_content .= '<div class="chapter">' . $chapter_content . '</div>';

		}

		// Wrap the chapters
		$chapters_content = '<div class="sa-ofsl-chapters">' . $chapters_content . '</div>';

		// Add the header
		if ( $args['display_header'] === true && ! empty( $args[ 'header' ] ) ) {
			$chapters_content = '<h2 id="chapters" class="sa-ofsl-chapters-header">' . $args[ 'header' ] . '</h2>' . $chapters_content;
		}

		// Wrap the content
		$chapters_content = '<div class="sa-ofsl-chapters-list">' . $chapters_content . '</div>';

	}

	// Add back the "Read More" filter
	add_filter( 'wp_trim_excerpt', 'sa_framework_add_read_more_to_excerpt', 10, 2 );

	// Restore original Post Data
	wp_reset_postdata();

	return $chapters_content;

});

//!  Register a UI for the shortcodes
if ( function_exists( 'shortcode_ui_register_for_shortcode' ) ) {

	shortcode_ui_register_for_shortcode(
		'print_sa_ofsl_chapters',
		array(
			'label'         => 'Print OFSL Chapters',
			'listItemImage' => 'dashicons-universal-access-alt',
			'post_type'     => array( 'page', 'councils' ),
			'attrs'         => array(
				array(
					'label'     => 'Which councils would you like to display? (Leave blank to show all councils)',
					'attr'      => 'councils',
					'type'      => 'post_select',
					'query'     => array( 'post_type' => 'councils' ),
					'multiple'  => true,
				),
				array(
					'label'     => 'Check if you want to add a header above the list.',
					'attr'      => 'display_header',
					'type'      => 'checkbox',
				),
				array(
					'label'     => 'Customize the header. (The default header is \'University of Alabama Chapters\'.',
					'attr'      => 'header',
					'type'      => 'text',
				),
			),
		)
	);

}