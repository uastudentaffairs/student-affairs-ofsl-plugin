<?php

// Sort chapters and councils by title in the admin
add_action( 'pre_get_posts', function( &$query ) {

	// Make sure we're in the admin and only tweaking the main query
	if ( is_admin() && $query->is_main_query() ) {

		// Only do if we're querying chapters or councils
		if ( isset( $query->query_vars[ 'post_type' ] ) && in_array( $query->query_vars[ 'post_type' ], array( 'chapters', 'councils' ) ) ) {

			// Set orderby (if one isn't specifically set
			if ( ! isset( $_GET[ 'orderby' ] ) ) {
				$query->set( 'orderby', 'post_title' );
			}

			// Set order (if one isn't specifically set
			if ( ! isset( $_GET[ 'order' ] ) ) {
				$query->set( 'order', 'ASC' );
			}

		}

	}

});

// Add options page and field groups
add_action( 'admin_menu', function() {

	// Use the ACF plugin for ease
	if ( function_exists( 'acf_add_options_page' ) ) {

		// Add the options page
		acf_add_options_page( array(
			'page_title' => __( 'OFSL Settings', 'uastudentaffairs' ),
			'menu_title' => __( 'OFSL Settings', 'uastudentaffairs' ),
			'menu_slug'  => 'ofsl-settings',
			'capability' => 'manage_ofsl_settings',
		) );

	}

	// Add field groups
	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'group_562aa933949a5',
			'title' => 'Front Page Footer Messages',
			'fields' => array (
				array (
					'key' => 'field_562aa93f99c05',
					'label' => 'Non-Discrimination Notice',
					'name' => 'sa_ofsl_non_disc_notice',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'basic',
					'media_upload' => 0,
				),
				array (
					'key' => 'field_562aa96f05011',
					'label' => 'Hazing and Harassment',
					'name' => 'sa_ofsl_hazing_notice',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'basic',
					'media_upload' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'ofsl-settings',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'left',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

	endif;

});