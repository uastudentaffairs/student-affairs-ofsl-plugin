<?php

// Set the chapters layout to full width
add_filter( 'sa_framework_page_layout', function( $defined_page_layout ) {
	if ( is_singular('chapters') ) {
		return 'full-width';
	}
	return $defined_page_layout;
});

// Set the section ID for councils
add_filter( 'sa_framework_page_section_post_id', function($page_section_post_id) {
	if ( is_singular( 'councils' ) ) {
		return 19;
	} else if ( is_singular( 'chapters' ) ) {
		return 21;
	}
	return $page_section_post_id;
});

// Filter the section header
add_filter( 'sa_framework_section_header', function( $page_section_title ) {
	if ( is_singular( 'councils' ) ) {
		return 'Councils';
	} else if ( is_singular( 'chapters' ) ) {
		return 'Chapters';
	}
	return $page_section_title;
});

add_filter( 'sa_framework_section_header_permalink', function($page_section_permalink) {
	if ( is_singular( 'councils' ) ) {
		return get_bloginfo( 'url' ) . '/councils/';
	} else if ( is_singular( 'chapters' ) ) {
		return get_bloginfo( 'url' ) . '/chapters/';
	}
	return $page_section_permalink;
});

// Show the page title for councils
add_filter( 'sa_framework_display_page_title', function( $display_page_title ) {
	return is_singular( 'councils' ) ? true : $display_page_title;
}, 100 );

// Add the page image header and council info after the title
add_action( 'sa_framework_before_content', function() {
	global $post;

	if ( is_singular() && ( $pg_featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'sa-page-featured-image' ) )
	     && ( $pg_featured_image_src = isset( $pg_featured_image[0] ) ? $pg_featured_image[0] : null ) ) {

		?><div class="sa-ofsl-featured-image"><img src="<?php echo $pg_featured_image_src; ?>" /></div><?php

	}

	// If a chapter, add their council
	if ( is_singular( 'chapters' ) ) {

		if ( $chapter_councils = get_post_meta( $post->ID, 'chapter_council', true ) ) {

			$chapter_council_string = array();
			foreach( $chapter_councils as $council_id ) {
				$chapter_council_string[] = '<a href="' . get_permalink($council_id) . '">' . get_the_title( $council_id ) . '</a>';
			}
			?><span class="chapter-council"><?php echo implode( ',', $chapter_council_string ); ?></span><?php

		}

	}

	// If a council, add their details
	else if ( is_singular( 'councils' ) ) {

		// Get council information
		$council_alias = strtoupper( get_post_meta( $post->ID, 'council_alias', true ) );
		$council_email = antispambot( get_post_meta( $post->ID, 'council_email', true ) );
		$council_website = get_post_meta( $post->ID, 'council_website', true );

		// Get social media
		$council_facebook = get_post_meta( $post->ID, 'facebook_url', true );
		$council_twitter = get_post_meta( $post->ID, 'twitter_handle', true );
		$council_instagram = get_post_meta( $post->ID, 'instagram_handle', true );

		?><div class="sa-ofsl-council-sidebar"><?php

		// Go to the chapters list
		?><a class="sa-ofsl-council-view-chapters button" href="#chapters">View UA Chapters</a><?php

		// Print contact info
		if ( $council_email || $council_website ) {

			// Build header
			$header = 'Contact Information';
			if ( $council_alias ) {
				$header = "Contact {$council_alias}";
			}

			?><div class="sa-ofsl-info">
			<h2 class="info-header"><?php echo $header; ?></h2>
			<ul class="info"><?php

				if ( $council_email ) {
					?><li class="email icon"><a href="mailto:<?php echo $council_email; ?>">Send <?php echo $council_alias ? $council_alias : 'them'; ?> an email</a></li><?php
				}

				if ( $council_website ) {
					?><li class="website icon"><a href="<?php echo $council_website; ?>" target="_blank">Visit <?php echo $council_alias ? "the {$council_alias}" : 'their'; ?> website</a></li><?php
				}

				?></ul>
			</div><?php

		}

		// Print social media
		if ( $council_facebook || $council_twitter || $council_instagram ) {

			// Build header
			$header = 'Follow on Social Media';
			if ( $council_alias ) {
				$header = "{$council_alias} on Social Media";
			}

			?><div class="sa-ofsl-info">
			<h2 class="info-header"><?php echo $header; ?></h2>
			<ul class="info"><?php

				if ( $council_facebook ) {
					?><li class="facebook icon"><a href="<?php echo $council_facebook; ?>" target="_blank">Follow on Facebook</a></li><?php
				}

				if ( $council_twitter ) {
					?><li class="twitter icon"><a href="https://twitter.com/<?php echo $council_twitter; ?>" target="_blank">Follow on Twitter</a></li><?php
				}

				if ( $council_instagram ) {
					?><li class="instagram icon"><a href="https://instagram.com/<?php echo $council_instagram; ?>" target="_blank">Follow on Instagram</a></li><?php
				}

				?></ul>
			</div><?php

		}

		?></div> <!-- .sa-ofsl-council-sidebar --><?php

	}

}, 0 );

// Adding Chapters wrap before content
add_action( 'sa_framework_before_page_title', function() {

	// Only for single chapters
	if ( ! is_singular( 'chapters' ) ) {
		return;
	}

	// Wrap the content
	echo '<div class="sa-ofsl-chapter">
		<div class="row">
			<div class="small-12 medium-8 medium-push-4 columns">
				<div class="chapter-content">';

}, 1 );

// Adding Chapters "after" content
add_action( 'sa_framework_after_content', function() {
	global $post;

	// Only for single chapters
	if ( ! is_singular( 'chapters' ) ) {
		return;
	}

	// Get chapter type
	$chapter_type = trim( get_post_meta( $post->ID, 'chapter_type', true ) );

	// Get chapter nickname
	$chapter_nickname = trim( get_post_meta( $post->ID, 'chapter_nickname', true ) );

	// Get chapter title
	$chapter_title = get_the_title();

	// If we have both, divide into columns
	?><div class="row row-chapter-details"><?php

	if ( $chapter_nickname && $chapter_type ) {

		?><div class="small-12 large-6 columns">
			<h2>Chapter Nickname</h2>
			<?php echo $chapter_nickname; ?>
		</div>
		<div class="small-12 large-6 columns">
			<h2>Chapter Type</h2>
			<?php echo $chapter_type; ?>
		</div><?php

	} else {

		?><div class="small-12 columns"><?php

			if ( $chapter_nickname ) {
				?><h2>Chapter Nickname</h2><?php
				echo $chapter_nickname;
			}

			else if ( $chapter_type ) {
				?><h2>Chapter Type</h2><?php
				echo $chapter_type;
			}

		?></div><?php

	}

	?></div><?php

	// Add addresses after the content
	$physical_address = sa_ofsl_get_address( $post->ID, 'physical' );
	$mailing_address = sa_ofsl_get_address( $post->ID, 'mailing' );

	// If we have both, divide into columns
	?><div class="row row-address"><?php

	if ( $physical_address && $mailing_address ) {

		?><div class="small-12 large-6 columns">
		<h2>Physical Address</h2>
		<?php echo $physical_address; ?>
		</div>
		<div class="small-12 large-6 columns">
		<h2>Mailing Address</h2>
		<?php echo $mailing_address; ?>
		</div><?php

	} else {

		?><div class="small-12 columns"><?php

		if ( $physical_address ) {
			?><h2>Physical Address</h2><?php
			echo $physical_address;
		}

		else if ( $mailing_address ) {
			?><h2>Mailing Address</h2><?php
			echo $mailing_address;
		}

		?></div><?php

	}

	?></div><?php

	// Only print the contact form if they have an email
	if ( $chapter_email = get_post_meta( $post->ID, 'chapter_email', true ) ) {

		// Build header
		$contact_form_header = 'Contact Information';
		if ( $chapter_title ) {
			$contact_form_header = "Contact {$chapter_title}";
		}

		?><h2><?php echo $contact_form_header; ?></h2><?php
		echo do_shortcode( '[gravityform id="1" title="false" description="false"]' );

	}

	// Close the content column and start the sidebar
	?></div> <!-- .chapter-content -->
		</div> <!-- .columns -->
	<div class="small-12 medium-4 medium-pull-8 columns"><?php

	// Get crest ID
	$crest_id = get_post_meta( $post->ID, 'chapter_crest', true );

	// Get crest image src
	$crest_image_src = ( $crest_image = wp_get_attachment_image_src( $crest_id, 'full' ) ) && isset( $crest_image ) && isset( $crest_image[0] ) ? $crest_image[0] : false;

	// Print crest
	?><div class="chapter-crest"><img src="<?php echo $crest_image_src; ?>" /></div><?php

	// Get chapter information
	$chapter_nickname = get_post_meta( $post->ID, 'chapter_nickname', true );
	$chapter_website = get_post_meta( $post->ID, 'chapter_website', true );

	// Get social media
	$chapter_facebook = get_post_meta( $post->ID, 'facebook_url', true );
	$chapter_twitter = get_post_meta( $post->ID, 'twitter_handle', true );
	$chapter_instagram = get_post_meta( $post->ID, 'instagram_handle', true );

	// Print social media
	if ( $chapter_facebook || $chapter_twitter || $chapter_instagram ) {

		?><div class="sa-ofsl-info">
		<h2 class="info-header">Social Media</h2>
		<ul class="info"><?php

			if ( $chapter_facebook ) {
				?><li class="facebook icon"><a href="<?php echo $chapter_facebook; ?>" target="_blank">Follow on Facebook</a></li><?php
			}

			if ( $chapter_twitter ) {
				?><li class="twitter icon"><a href="https://twitter.com/<?php echo $chapter_twitter; ?>" target="_blank">Follow on Twitter</a></li><?php
			}

			if ( $chapter_instagram ) {
				?><li class="instagram icon"><a href="https://instagram.com/<?php echo $chapter_instagram; ?>" target="_blank">Follow on Instagram</a></li><?php
			}

			?></ul>
		</div><?php

	}

	// For more information...
	if ( $chapter_website ) {

		?><div class="sa-ofsl-info">
		<h2 class="info-header">For More Information</h2>
		<ul class="info"><?php

			if ( $chapter_website ) {
				?><li class="website icon"><a href="<?php echo $chapter_website; ?>" target="_blank">Visit <?php echo $chapter_nickname ? "the {$chapter_nickname}" : 'their'; ?> website</a></li><?php
			}

			?></ul>
		</div><?php

	}

	// Go to the chapters page
	?><a class="sa-ofsl-council-view-chapters button expand" href="<?php echo get_bloginfo('url'); ?>/chapters/">View other chapters</a><?php

	// Close the columns and chapter
	?></div> <!-- .columns -->
		</div> <!-- .row -->
	</div> <!-- .sa-ofsl-chapter --><?php

}, 1 );

// Add photo to front page
add_action( 'sa_framework_before_content', function() {

	// Only on front page
	if ( ! is_front_page() ) {
		return;
	}

	?><img src="https://ofsl.sa.ua.edu/wp-content/uploads/UA-OFSL-Bid-Day-Cards-for-home.jpg" /><?php

});

// Add disclaimers after content on front page
add_action( 'sa_framework_after_front_main', function() {

	// Get non-discrimination notice
	$sa_ofsl_non_disc_notice = get_option( 'options_sa_ofsl_non_disc_notice' );

	// Get hazing notice
	$sa_ofsl_hazing_notice = get_option( 'options_sa_ofsl_hazing_notice' );

	// If we have neither, get out of here
	if ( ! $sa_ofsl_non_disc_notice ) {
		return;
	}

	?><div id="sa-ofsl-front-page-disclaimer">
	<div class="row"><?php

		// The columns will be determined by what we have
		$column_classes = ( $sa_ofsl_non_disc_notice && $sa_ofsl_hazing_notice) ? 'small-12 medium-6 columns' : 'small-12 columns';

		// Print non-discrimination notice
		if ( $sa_ofsl_non_disc_notice ) {
			?><div class="<?php echo $column_classes; ?>">
			<h2>Non-Discrimination Notice</h2><?php
			echo wpautop( $sa_ofsl_non_disc_notice );
			?></div><?php
		}

		// Print hazing notice
		if ( $sa_ofsl_hazing_notice ) {
			?><div class="<?php echo $column_classes; ?>">
			<h2>Hazing and Harassment</h2><?php
			echo wpautop( $sa_ofsl_hazing_notice );
			?></div><?php
		}

		?></div>
	</div><?php

});

// Change the chapter ID field value to the title before it's saved
add_filter( 'gform_save_field_value', function( $value, $entry, $field, $form, $input_id ) {

	// Only dealing with the Contact Us form
	if ( $form['id'] != 1 ) {
		return $value;
	}

	// Only for the chapter field
	if ( 'Chapter' != $field->adminLabel ) {
		return $value;
	}

	// Only if the value is an ID
	if ( is_numeric( $value ) && $value > 0 ) {

		// Return chapter title
		if ( $contact_chapter_title = get_the_title( $value ) ) {
			return $contact_chapter_title;
		}

	}

	return $value;

}, 10, 5 );

// Tweak the contact us notification depending on selected chapter
add_filter( 'gform_notification_1', function( $notification, $form, $entry ) {

	// Go through fields and find specific chapter
	$contact_chapter_id = 0;
	foreach ( $form['fields'] as $field ) {
		if ( 'Chapter' == $field->adminLabel ) {

			// Get the contact chapter ID
			$contact_chapter_id = 0;

			// First check the entry title
			$entry_chapter_title = rgar( $entry, $field['id'] );

			// If entry title is the ID
			if ( $entry_chapter_title && is_numeric( $entry_chapter_title ) && $entry_chapter_title > 0 ) {

				// Set ID
				$contact_chapter_id = $entry_chapter_title;

				// Update value with chapter title from ID
				if ( $contact_chapter_title = get_the_title( $contact_chapter_id ) ) {
					$entry[ $field[ 'id' ] ] = $contact_chapter_title;
				}

			}

			// If the entry title is the title, we need the ID
			else if ( $entry_chapter_title
			          && ( $contact_chapter = get_page_by_title( $entry_chapter_title, OBJECT, 'chapters' ) )
			          && ( isset( $contact_chapter->ID ) ) ) {

				// Get ID from post object
				$contact_chapter_id = $contact_chapter->ID;

			}

			// If we still don't have the ID, check $_POST object
			if ( ! $contact_chapter_id && isset( $_POST[ "input_{$field['id']}" ] ) && $_POST[ "input_{$field['id']}" ] > 0 ) {

				// Get ID from $_POST
				$contact_chapter_id = $_POST[ "input_{$field['id']}" ];

				// Update value with chapter title
				if ( $contact_chapter_title = get_the_title( $contact_chapter_id ) ) {
					$entry[ $field[ 'id' ] ] = $contact_chapter_title;
				}

			}

		}

	}

	// If we have a chapter ID, see if we have a chapter email
	if ( $contact_chapter_id > 0 && ( $chapter_email = get_post_meta( $contact_chapter_id, 'chapter_email', true ) ) ) {

		// Convert "to" to an array
		if ( ! is_array( $notification[ 'to' ] ) ) {
			$notification[ 'to' ] = explode( ',', $notification[ 'to' ] );
		}

		// Add the chapter email
		$notification[ 'to' ][] = $chapter_email;

		// Return to string format
		$notification[ 'to' ] = implode( ',', $notification[ 'to' ] );

	}

	return $notification;

}, 10, 3 );

// Tweak the contact form
// Adds chapter options. On the front-end, hides chapter depending on the page
add_filter( 'gform_pre_render', function( $form ) {
	global $post;

	// Only dealing with the Contact Us form
	if ( $form['id'] != 1 ) {
		return $form;
	}

	// If we're on a chapter page, only get that chapter
	$contact_chapter_id = is_singular( 'chapters' ) && $post->ID ? $post->ID : false;

	// Loop through the fields
	foreach ( $form['fields'] as &$field ) {

		// Hide the chapter field
		if ( 'Chapter' == $field->adminLabel ) {

			// Hide this field because it's dynamically selected
			if ( ! is_admin() ) {
				$field->cssClass .= ' gf_hidden';
			}

			// Setup chapter args
			$chapter_args = array(
				'posts_per_page'   => -1,
				'orderby'          => 'title',
				'order'            => 'ASC',
				'post_type'        => 'chapters',
				'suppress_filters' => true
			);

			// If we're on a chapter page, only get that chapter
			if ( $contact_chapter_id ) {
				$chapter_args[ 'include' ] = array( $contact_chapter_id );
			}

			// Add the chapter choices
			if ( $chapters = get_posts( $chapter_args ) ) {

				// If we're on a chapter page, it's the only result
				if ( $contact_chapter_id ) {
					$field->choices = array();
				}

				// Add each chapter to the choices
				foreach( $chapters as $chapter ) {

					// Setup chapter array
					$chapter_array = array(
						'text' => $chapter->post_title,
						'value' => $chapter->post_title,
					);

					// Select the current chapter
					if ( $contact_chapter_id ) {
						$chapter_array[ 'isSelected' ] = true;
					}

					// Add to choices array
					$field->choices[] = $chapter_array;

				}

			}

		}

	}

	return $form;

}, 1000 );