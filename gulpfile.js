var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');

gulp.task('sass', function() {
	gulp.src('scss/sa-ofsl.scss')
		.pipe(sass({outputStyle:'compressed'}))
		.pipe(gulp.dest('css'));
});

gulp.task('default', ['sass'], function() {
	gulp.watch(['scss/sa-ofsl.scss'],['sass']);
});